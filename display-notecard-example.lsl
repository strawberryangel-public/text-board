string      name;
integer     line;
key         queryHandle;
key         cardUuid;

displayCard()
{
    name = llGetInventoryName(INVENTORY_NOTECARD, 0);

    if (llGetInventoryType(name) != INVENTORY_NONE)
    {
        llSay(0, "Attempting to read and display the notecard named " + name + ".");
        llMessageLinked(LINK_ROOT, 101988, "", NULL_KEY); // Select bold font
        llMessageLinked(LINK_ROOT, 101995, "", NULL_KEY); // Clear display
        line = 0;
        queryHandle = llGetNotecardLine(name, line);
        cardUuid = llGetInventoryKey(name);
    }
}

showHelp()
{
    llSay(0, "Drop a notecard in. If the notecard can be read, it will be displayed on the display board.");
}

default
{
    changed(integer intChange)
    {
        if (intChange & CHANGED_INVENTORY)
        {   
            displayCard();
        }
    }

    dataserver(key id, string data)
    {
        if (id == queryHandle)
        {
            if (data == EOF)
            {
                llSay(0, "Done reading notecard. Removing it.");
                llRemoveInventory(name);
                return;
            }

            llMessageLinked(LINK_ROOT, 102000 + line, data, NULL_KEY);
            queryHandle = llGetNotecardLine(name, ++line);
        }
    }

    state_entry()
    {
        showHelp();
    }

    touch_start( integer num_detected )
    {
        showHelp();
    }
}
