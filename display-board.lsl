// Information originating from this script
#define DISPLAY_BOARD_TO_EXTERNAL_SCRIPT_NUMBER 101000
#define BOOTING_MESSAGE "STARTING"
#define READY_MESSAGE "READY"

// Information originating from others scripts
#define EXTERNAL_SCRIPT_TO_DISPLAY_BOARD_NUMBER 102000
#define OFFSET_SHOW_TEST_PATTERN -1
#define OFFSET_FULLBRIGHT_ON -2
#define OFFSET_FULLBRIGHT_OFF -3
#define OFFSET_SET_COLOR -4
#define OFFSET_CLEAR_ALL -5
#define OFFSET_CLEAR_BODY -6

#define OFFSET_FONT_SET1 -11
#define OFFSET_FONT_SET2 -12
#define OFFSET_FONT_SET3 -13
#define OFFSET_FONT_SET4 -14
#define OFFSET_FONT_SET5 -15
#define OFFSET_FONT_SET6 -16
#define OFFSET_FONT_SET7 -17
#define OFFSET_FONT_SET8 -18
#define OFFSET_FONT_SET9 -19

#define DEFAULT_FULLBRIGHT FALSE

// This font is lighter weight.
#define FONT_SET1 1
// This font is heavier weight.
#define FONT_SET2 2
// Medieval Font
#define FONT_SET3 3
// Marker Font
#define FONT_SET4 4
// South Western Font
#define FONT_SET5 5
// Bold Serif Font
#define FONT_SET6 6
// Dripping Font
#define FONT_SET7 7
// Metal Font
#define FONT_SET8 8
// Future Font
#define FONT_SET9 9

#define DEFAULT_FONT_SET FONT_SET6

// The shape of the board is described here
#define CHARACTER_PER_PRIM 8
#define PRIMS_PER_LINE 5
#define DISPLAY_TITLE_LINES 1
#define DISPLAY_BODY_LINES 10

#define CHARACTERS_PER_LINE (CHARACTER_PER_PRIM * PRIMS_PER_LINE)
#define TOTAL_LINES (DISPLAY_TITLE_LINES + DISPLAY_BODY_LINES)

////////////////////////////////////////
// Font information; don't change.
////////////////////////////////////////

#define FONT_SET12_CHARACTERS "" \
            + " !\"#$%&'()" \
            + "*+,-./0123" \
            + "456789:;<=" \
            + ">?@ABCDEFG" \
            + "HIJKLMNOPQ" \
            + "RSTUVWXYZ[" \
            + "\\]^_`abcde" \
            + "fghijklmno" \
            + "pqrstuvwxy" \
            + "z{|}~"

////////////////////////////////////////
// State Variables
////////////////////////////////////////

// Current font texture.
key fontTexture;
integer fontTextureRows;
integer fontTextureColumns;
vector fontCell;
vector fontColor = <1.0, 1.0, 1.0>; 
// This hold the sequence of characters in the font texture.
string charactersInTexture;
// Map from display position to link number.
list links = [];
// Line of spaces to clear out text at the end of the line.
string lineOfSpaces;
// Full bright text
integer fullbright = DEFAULT_FULLBRIGHT;

integer isReady = FALSE;

////////////////////////////////////////
// Startup Functions
////////////////////////////////////////

announceReady()
{
    isReady = TRUE;
    llSay(0, "Display board ready.");
    llMessageLinked(LINK_SET, DISPLAY_BOARD_TO_EXTERNAL_SCRIPT_NUMBER, READY_MESSAGE, NULL_KEY);
}

announceStarting()
{
    isReady = FALSE;
    llSay(0, "Display board starting.");
    llMessageLinked(LINK_SET, DISPLAY_BOARD_TO_EXTERNAL_SCRIPT_NUMBER, BOOTING_MESSAGE, NULL_KEY);
}

init()
{
    announceStarting();
    discoverLinks();
    setFont(DEFAULT_FONT_SET);
    showTestPattern();
    announceReady();
}

/**
 * Determine which link maps to which part of the display.
 * Each display segment has a name with the form "Ann"
 * Where "nn" is a two-digit value that represents the index
 * in the `links` array.
 */
discoverLinks()
{
    // Create a line of spaces.
    integer i = CHARACTERS_PER_LINE;
    lineOfSpaces = "";
    while(i-- > 0) lineOfSpaces += " ";

    // Pre-configure the list of links.
    // All values default to -1 to indicate no prim for this position.
    links = [];
    integer numberOfLinks = PRIMS_PER_LINE * TOTAL_LINES;
    while(numberOfLinks-- > 0) links += [-1];

    integer count = llGetNumberOfPrims();
    while(count > 0) {
        string name = llGetLinkName(count);
        integer len = llStringLength(name);
        if(len == 3) {
            if(llGetSubString(name, 0, 0) == "A") {
                string num = llGetSubString(name, 1, 2);
                if(num == "00") {
                    setLink(0, count);
                } else {
                    integer index = integer(num);
                    if(index > 0) {
                        setLink(index, count);
                    }
                }
            }
        } 

        count--;
    }
}

setLink(integer index, integer linkNumber)
{
    links = llListReplaceList(links, [linkNumber], index, index);
}

////////////////////////////////////////
// Display Functions
////////////////////////////////////////

clearLines(integer startRow, integer stopRow)
{
    integer i = startRow;
    while(i < stopRow)
    {
        displayLine(i, "");
        i++;
    }
}

/**
 * Display a single character.
 * This involves adjusting the font texture on a prim face.
 */
displayCharacter(integer link, integer face, float x, float y, integer fullbright)
{
     llSetLinkPrimitiveParamsFast(link, [
        PRIM_TEXTURE,
        face,
        fontTexture,
        fontCell,
        <x, y, 0.0>,
        0,
        PRIM_FULLBRIGHT,
        face,
        fullbright,
        PRIM_COLOR,
        face,
        fontColor,
        1.0
     ]);
}

/**
 * Display a line of text.
 * This involves discovering the prims in the line
 * and applying text to each prim.
 */
displayLine(integer line, string message)
{
    message = llGetSubString(message + lineOfSpaces, 0, CHARACTERS_PER_LINE - 1);

    integer i = 0;
    while(i < PRIMS_PER_LINE)
    {
        string primText = llGetSubString(message, CHARACTER_PER_PRIM*i, CHARACTER_PER_PRIM*(i+1)-1);
        integer prim_index = PRIMS_PER_LINE*line + i;
        integer link = llList2Integer(links, prim_index);
        if(link >= 0)
            primString(link, primText);
        i++;
    }
}

/**
 * Fill a prim with text.
 * If the text is shorter than the prim's ability to display,
 * fill the rest of the prim with spaces.
 */
primString(integer link, string chars)
{
    if(llStringLength(chars) < CHARACTER_PER_PRIM)
        chars = llGetSubString(chars + lineOfSpaces, 0, CHARACTER_PER_PRIM - 1);

    integer i = 0;
    while(i < CHARACTER_PER_PRIM)
    {
        string character = llGetSubString(chars, i, i);

        // decodeCharacter(char);
        // BEGIN Inlining decodeCharacter 
        float decode_character_x;
        float decode_character_y;

        // Find character in list of font characters,
        // falling back to space.
        if(character == "") character = " ";
        integer index = llSubStringIndex(charactersInTexture, character);
        if(index < 0) index = llSubStringIndex(charactersInTexture, " ");

        float dx = (index % fontTextureColumns) / (float)fontTextureColumns;
        float dy = (index / fontTextureRows) / (float)fontTextureRows;
        float x0 = -(fontTextureColumns-1)/2.0 / (float)fontTextureColumns; // -0.45;
        float y0 =  (fontTextureRows-1)/2.0 / (float)fontTextureRows; // 0.45;
        decode_character_x =  dx + x0;
        decode_character_y = -dy + y0;
        // END Inlining decodeCharacter

        displayCharacter(link, i, decode_character_x, decode_character_y, fullbright);
        i++;
    }
}

/**
 * Show the default test pattern for the board.
 */
showTestPattern()
{
    displayLine(0,  "Display Board 1234567890");
    displayLine(1,  "Line #1 1234567890123456789 !\"#$%&'()012");
    displayLine(2,  "Line #2 1234567890123456789*+,-./0123012");
    displayLine(3,  "Line #3 1234567890123456789456789:;<=012");
    displayLine(4,  "Line #4 1234567890123456789>?@ABCDEFG012");
    displayLine(5,  "Line #5 1234567890123456789HIJKLMNOPQ012");
    displayLine(6,  "Line #6 1234567890123456789RSTUVWXYZ[012");
    displayLine(7,  "Line #7 1234567890123456789\\]^_`abcde012");
    displayLine(8,  "Line #8 1234567890123456789fghijklmno012");
    displayLine(9,  "Line #9 1234567890123456789pqrstuvwxy012");
    displayLine(10, "Line #10 234567890123456789z{|}456789012");
}

/**
 * Change the font settings.
 */
setFont(integer fontSet)
{
    switch(fontSet)
    {
        case FONT_SET1:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "e9623c61-579e-b1ac-c0e5-fd3a1209558d";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET2:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "9f3a0cad-cdc6-f186-371e-5876837a5b3b";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET3:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "e002868b-4a02-9e67-30cb-4d925d0958e4";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET4:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "37d3b2e1-93f6-42a8-8f17-1725d34c64b9";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET5:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "780547c1-edda-bd71-ab6d-e3ca04ff595d";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET6:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "a244ab00-d246-f8ee-fd85-2b6e03e81bed";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET7:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "68b39c57-0d61-804d-0682-cf18ca3e4899";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET8:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "598580db-276c-8de0-53ee-db428bd43f51";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        case FONT_SET9:
            fontTextureRows = 10;
            fontTextureColumns = 10;
            fontCell = <1.0/fontTextureColumns, 1.0/fontTextureRows, 0.0>;
            fontTexture = "b6e974e2-b786-3128-2c6b-c4d66519d4bc";
            // Characters in the font grid.
            charactersInTexture = FONT_SET12_CHARACTERS;
            break;
        default:
            setFont(DEFAULT_FONT_SET);
            break;
    }
}

default
{
    on_rez( integer start_param)
    {
        llResetScript();
    }

    state_entry()
    {
        init();
    }

    link_message( integer sender_num, integer num, string str, key id )
    {
        integer offset = num - EXTERNAL_SCRIPT_TO_DISPLAY_BOARD_NUMBER;

        // This should be the most common case.
        if(0 <= offset && offset < TOTAL_LINES) {
            displayLine(offset, str);
            return;
        }

        switch(offset)
        {
            case OFFSET_FULLBRIGHT_ON:
                fullbright = TRUE;
                break;
            case OFFSET_FULLBRIGHT_OFF:
                fullbright = FALSE;
                break;
            case OFFSET_SHOW_TEST_PATTERN:
                showTestPattern();
                break;
            case OFFSET_SET_COLOR:
                fontColor = vector(str);
                break;
            case OFFSET_CLEAR_ALL:
                clearLines(0, TOTAL_LINES);
                break;
            case OFFSET_CLEAR_BODY:
                clearLines(DISPLAY_TITLE_LINES, TOTAL_LINES);
                break;
            case OFFSET_FONT_SET1:
                setFont(FONT_SET1);
                break;
            case OFFSET_FONT_SET2:
                setFont(FONT_SET2);
                break;
            case OFFSET_FONT_SET3:
                setFont(FONT_SET3);
                break;
            case OFFSET_FONT_SET4:
                setFont(FONT_SET4);
                break;
            case OFFSET_FONT_SET5:
                setFont(FONT_SET5);
                break;
            case OFFSET_FONT_SET6:
                setFont(FONT_SET6);
                break;
            case OFFSET_FONT_SET7:
                setFont(FONT_SET7);
                break;
            case OFFSET_FONT_SET8:
                setFont(FONT_SET8);
                break;
            case OFFSET_FONT_SET9:
                setFont(FONT_SET9);
                break;
        }
    }
}
