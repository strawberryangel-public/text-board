# Text Board

## Overview

This is a generic ASCII text board
controlled via link messages.

* The display title line has 24 characters.
* The next ten display body lines have 40 characters.

## Board Operation

### Startup

When the `display-board.lsl` script starts,
it sends a link message number 101000 with the text `STARTING`.
It then will take a moment to auto-configure itself.

When the board is ready,
it sends a link message number 101000 with the text `READY`.

### Displaying Text

Link messages control the display.

The link message number controls which line is altered:

Link Number | Line
:--- | :---
102000 | Set the display title.
102001 | Set the first line of the display body.
102002 | Set the second line of the display body.
... | ... etc. ...
102010  | Set the last (tenth) line of the display body.

In other words,
the number 102000 + n will update the nth body line.

### Display Test Pattern

A test pattern can be shown by sending a link message.

Link Number | Action
:--- | :---
101999 | Show the test pattern.

### Text Full Bright

Full bright for the text can be enabled or disabled by sending link messages.
This affects future text sent.

Link Number | Action
:--- | :---
101998 | Turns text full bright on.
101997 | Turns text full bright off.

### Set Text Color

Future text color can be set by sending a link message.
The `str` parameter of `llMessageLinked` must contain the color vector as a string.
This affects future text sent.

Link Number | Action
:--- | :---
101996 | Sets the text color.

### Clear Display

Link Number | Action
:--- | :---
101995 | Clear the entire display.
101994 | Clear the body of the display.

### Select Font

There are a few fonts that come with the display board.
The font for future text can be selected by sending a link message.

Link Number | Font
:--- | :---
101989 | Standard font
101988 | Bold standard font
101987 | Medieval font
101986 | Marker font
101985 | Southwestern font
101984 | Bold serif font
101983 | Dripping font
101982 | Metal font
101981 | Future font

## Script Details

The script uses Firestorm script extensions to
allow for the use of
`#define`, `#include` and `//` comments.

### Script Communication

#### Information To Other Scripts

The link number broadcast while the script is starting can be altered
by changing the line:

```cpp
#define DISPLAY_BOARD_TO_EXTERNAL_SCRIPT_NUMBER 101000
```

#### Other Scripts To This Script

The link message number to update the board can be altered
by changing the line:

```cpp
#define EXTERNAL_SCRIPT_TO_DISPLAY_BOARD_NUMBER 102000
```

All commands to the display board are relative to this number:

Link Number | Action
:--- | :---
... | ...
n - 12 | Select font set 2.
n - 11 | Select font set 1.
... | ...
n - 6 | Clear the display body text.
n - 5 | Clear the entire display.
n - 4 | Set the color.
n - 3 | Turn full bright off.
n - 2 | Turn full bright on.
n - 1 | Display test pattern.
n + 0 | Set title text.
n + 1 | Set first line of the body text.
n + 2 | Set second line of the body text.
... | ...
n + 10 | Set last line of the body text.

### Display Size

The size of the board can be altered by adding or removing
display prims and updating their names.
The script assumes a rectangular layout in the way
described in *Object Details*.
The following lines control the shape of the board:

```cpp
// The shape of the board is described here
#define CHARACTER_PER_PRIM 8
#define PRIMS_PER_LINE 5
#define DISPLAY_TITLE_LINES 1
#define DISPLAY_BODY_LINES 10
```

## Object Details

A single ordinary prim is the root object.

The character display is made up of mesh prims
with eight square rectangles to hold eight characters.

The name of each prim indicates its position on the board.
The names are of the format `Ann` where `nn` indicates its
index in a large grid of characters.

The grid of characters is `PRIMS_PER_LINE` prims wide and
`TOTAL_LINES` high. Currently those values are **5** and **11**.
The upper left corner is `A00`. 
Moving left to right:

* The title row is `A00` `A01` `A02`.
  The title only has 24 characters, i.e. three prims.
  The grid is assumed to be five wide; 
  it ignores the missing prims `A03` and `A04`.
* The first body row is `A05` through `A09`,
  40 characters on five prims.
* The second body row is `A10` through `A14`.
* The third body row is `A15` through `A19`.
